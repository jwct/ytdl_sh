#!/bin/bash
CURRENT_PATH=`pwd`
OUT_PATH=$CURRENT_PATH/out

S3_PATH=$1
FILE_NAME=$2


aws s3 cp $OUT_PATH/$FILE_NAME s3://$S3_PATH/$FILE_NAME  --region ap-east-1 
