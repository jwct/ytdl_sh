#!/bin/bash
CURRENT_PATH=`pwd`
SCRIPT_PATH=$CURRENT_PATH/scripts
MP4_PATH=$CURRENT_PATH/mp4
OUT_PATH=$CURRENT_PATH/out

YT_ID=$1
S3_PATH=$2

$SCRIPT_PATH/ytdl.sh $YT_ID && $SCRIPT_PATH/convertMp4.sh $MP4_PATH/$YT_ID.aac && $SCRIPT_PATH/s3.sh $OUT_PATH/$YT_ID.m4a $S3_PATH
