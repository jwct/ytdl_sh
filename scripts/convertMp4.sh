#!/bin/bash
CURRENT_PATH=`pwd`
OUT_PATH=$CURRENT_PATH/out
BIN_PATH=$CURRENT_PATH/bin

echo 'Input file:' $1

filename=`basename  $1 | cut -d. -f1`

echo 'Output file:' $OUT_PATH/$filename.m4a

$BIN_PATH/ffmpeg/ffmpeg -i $1 -b:a 32k -ac 1  $OUT_PATH/$filename.m4a 
