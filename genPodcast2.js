import ytpl from 'ytpl';
import jsonfile from 'jsonfile'

var args = process.argv;

const settingFile = args[2];
// console.log(settingFile);
if (args.length <= 2) {
    console.error("no setting file");
    process.exit();
}
const setting = jsonfile.readFileSync(settingFile) 

const getPlaylist = async () => {
    return await ytpl(setting.playListDownload);
}

getPlaylist()
.then( (playlist)=> {    
        console.log('\n# cd '+setting.binPath);
        console.log('./scripts/install_cli.sh ');
        console.log('# ./s3_clean.sh ');
        console.log('# ./clean.sh ');
        playlist.items.forEach((item) => console.log(`./scripts/ytdl_all.sh ${item.id} ${setting.s3DataPath} `));
        console.log(`./scripts/s3_podcast.sh ${setting.s3Path} ${setting.xmlFilename}`);
    });
