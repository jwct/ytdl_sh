import { Podcast } from 'podcast';
import ytpl from 'ytpl';
import jsonfile from 'jsonfile'

var args = process.argv;

const settingFile = args[2];
if (args.length <= 2) {
    console.error("no setting file");
    process.exit();
}
// console.log(settingFile);
const setting = jsonfile.readFileSync(settingFile) 

const FEED_NAME = setting.feedName;
const IMAGE_URL = setting.imageUrl;
const FEED_URL = setting.feedUrl;

const feed = new Podcast({
    title: FEED_NAME
    , description: FEED_NAME
    , feed_url: FEED_URL
    , site_url: FEED_URL
    , image_url: IMAGE_URL
    , docs: FEED_URL
    , categories: ['CATEGORY']
    , pubDate: Date.now()
    , itunesSubtitle: FEED_NAME
    , itunesSummary: FEED_NAME
    , ituneBucketsExplicit: false
    , itunesCategory: [
      { "text": "CATEGORY" }
    ]
  });
const getCustomItems = (items) => {
    const customItems = [
        // {
        //     "title":"大學堂 : 從空間史探究1941年的香港保衛戰 (下) / 鄺智文(香港浸會大學歷史系副教授)"
        // , "id":"1069_2204071448_22639.mp3"
        // , "_url" : "https://podcast.rthk.hk/podcast/media/radio1_university_knowledge_platform/1069_2204071448_22639.mp3"    
        // }
    ];

    return items.concat(customItems);
}
const genPodcast = (items) => {
    items.forEach(
        (item) => {
            if (!item._url) {
                item._url=`${setting.mediaFolder}${item.id}.m4a`;
            }            
            feed.addItem({
                title: `${item.title}`
                , url: `${setting.mediaFolder}${item.id}.m4a`
                , guid: item.id
                , date: Date.now()
                , categories: ['podcast']
                , itunesExplicit: false
                , itunesSubtitle: `${item.author.name}: ${item.title}`
                , itunesSummary: `${item.author.name}: ${item.title}`
                , itunesDuration: 0
                , itunesKywords: ['podcast']
                , enclosure: { url: item._url }
            });
    });

    var xml = feed.buildXml('\t');
    console.log(xml);
}
const getPlaylist = async () => {
    return await ytpl(setting.playListFeed);
}
getPlaylist()
.then( (playlist) => getCustomItems(playlist.items) )
.then( (items)=>genPodcast(items) );

